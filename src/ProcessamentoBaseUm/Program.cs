﻿using System;
using Microsoft.Extensions.Configuration;
using System.IO;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using Newtonsoft.Json;
using ProcessamentoBaseUm.Models;

namespace ProcessamentoBaseUm
{
    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var configurations = config.GetSection("RabbitMQConfigurations");

            var _connectionFactory = new ConnectionFactory()
            {
                HostName = configurations.GetSection("HostName").Value,
                Port = int.Parse(configurations.GetSection("Port").Value),
                UserName = configurations.GetSection("UserName").Value,
                Password = configurations.GetSection("Password").Value,
                VirtualHost = configurations.GetSection("VirtualHost").Value,
                Protocol = Protocols.DefaultProtocol
            };

            using (var connection = _connectionFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "fila-base2",
                                         durable: false, //quando eu desligo ou reinicio 
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += Consumer_Received;

                channel.BasicConsume(queue: "fila-base2",
                     autoAck: true,
                     consumer: consumer);



                Console.WriteLine("Pressione uma tecla para encerrar...");
                Console.ReadKey();
            }
        }

        private static void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {

            var message = Encoding.UTF8.GetString(e.Body);
            Console.WriteLine(Environment.NewLine + "[Nova mensagem recebida] " + message);
            var obj = JsonConvert.DeserializeObject<Heroi>(message);
            Persistir.SalvarHeroi(obj);

        }
    }
}
