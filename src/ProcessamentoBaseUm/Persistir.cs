﻿using Microsoft.Extensions.Configuration;
using ProcessamentoBaseUm.Models;
using System;
using System.IO;
using Dapper;
using System.Data.SqlClient;

namespace ProcessamentoBaseUm
{
    public static class Persistir
    {
        
        public static void SalvarHeroi(Heroi entidade)
        {

            var _configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            using (SqlConnection conexao = new SqlConnection(
                _configuration.GetConnectionString("Base2Connection")))
            {
                Console.WriteLine("Incluindo registro na base 2");
                conexao.Open();
                string sql = "INSERT INTO heroi (UK_ID, Nome) Values (@Id, @Nome);";
                var affectedRows = conexao.Execute(sql, entidade);

                if (affectedRows > 0)
                {
                    Console.WriteLine("Registro incluído com sucesso");
                }

                conexao.Close();
            }
        }
    }
}
