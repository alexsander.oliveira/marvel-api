﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessamentoBaseUm.Models
{
    public class Heroi
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
    }
}
