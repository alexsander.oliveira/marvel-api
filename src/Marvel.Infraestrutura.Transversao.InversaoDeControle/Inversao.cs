﻿using System;
using Marvel.Dominio.Interfaces.Servicos;
using Marvel.Dominio;
using Microsoft.Extensions.DependencyInjection;
using Marvel.Dominio.Interfaces.Repositorios;
using Marvel.Infraestrutura.Dados.Repositorio.Repositorios;
using Marvel.Infraestrutura.Dados.Repositorio.Contexto;
using Marvel.Dominio.Servicos;
using Marvel.Dominio.Entidades;
using Marvel.Aplicacao.Interfaces;
using Marvel.Aplicacao.Servicos;
using Marvel.Integracao.Integracoes.Interfaces;
using Marvel.Integracao.Integracoes;
using Marvel.Aplicacao.Mapeamentos;
using AutoMapper;

namespace Marvel.Infraestrutura.Transversao.InversaoDeControle
{
    public class Inversao
    {
        public static void RegistrarServicos(IServiceCollection services)
        {
            //Autommaper
            services.AddSingleton<IConfigurationProvider>(AutoMapperConfiguracao.RegistrarMappings());
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));

            //Infra
            services.AddScoped<Context>();

            services.AddScoped<IHeroiRepositorio, HeroiRepositorio>();
            services.AddScoped<IRepositorioBase<Heroi>, RepositorioBase<Heroi>>();

            //Dominio
            services.AddScoped<IHeroiService, HeroiService>();
            services.AddScoped<IServiceBase<Heroi>, ServiceBase<Heroi>>();

            //Aplicação
            services.AddScoped<IHeroiAplicacaoService, HeroiAplicacaoService>();
            services.AddScoped<IAplicacaoServiceBase<Heroi>, AplicacaoServiceBase<Heroi>>();

            //Integração
            services.AddScoped<IServicoBroker, ServicoBroker>();

        }
    }
}
