﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Infraestrutura.Transversal.Dto.Integracoes
{
    public class MensagemDto
    {
        public string Mensagem { get; set; }
        public string Fila { get; set; }
        public string Exchange { get; set; }
    }
}
