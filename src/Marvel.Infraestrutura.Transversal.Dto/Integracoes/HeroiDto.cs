﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Infraestrutura.Transversal.Dto.Integracoes
{
    public class HeroiDto
    {
        public string Nome { get; set; }
    }
}
