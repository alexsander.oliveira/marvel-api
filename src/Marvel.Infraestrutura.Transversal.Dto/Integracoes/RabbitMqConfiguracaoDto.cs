﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Infraestrutura.Transversal.Dto.Integracoes
{
    public class RabbitMqConfiguracaoDto
    {
        public string HostName { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
    }
}
