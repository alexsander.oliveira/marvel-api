﻿using Marvel.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Interfaces.Repositorios
{
    public interface IRepositorioBase<T> : IDisposable where T : class
    {
        void Adicionar(T entidade);
        void Alterar(T entidade);
        void Excluir (Guid id);
        IEnumerable<T> ObterTodos();
        T ObterPeloId(Guid id);
        int SaveChanges();
    }
}
