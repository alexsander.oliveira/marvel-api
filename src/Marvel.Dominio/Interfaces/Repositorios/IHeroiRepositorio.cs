﻿using Marvel.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Interfaces.Repositorios
{
    public interface IHeroiRepositorio: IRepositorioBase<Heroi>
    {

    }
}
