﻿using Marvel.Dominio.Entidades;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;

namespace Marvel.Dominio.Interfaces.Servicos
{
    public interface IHeroiService: IServiceBase<Heroi>
    {
        void SalvarIntegracao(MensagemDto dto);
    }
}
