﻿using Marvel.Dominio.Interfaces.Repositorios;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Interfaces.Servicos
{
    public interface IServiceBase<T> where T : class
    {
        void Adicionar(T entidade);
        void Alterar(T entidade);
        void Excluir(Guid id);
        IEnumerable<T> ObterTodos();
        T ObterPeloId(Guid id);
    }
}
