﻿using Marvel.Dominio.Entidades;
using Marvel.Dominio.Interfaces.Repositorios;
using Marvel.Dominio.Interfaces.Servicos;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using Marvel.Integracao.Integracoes.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Servicos
{
    public class HeroiService : ServiceBase<Heroi>, IHeroiService
    {
        public readonly IServicoBroker _servicoBroker;
        public readonly IHeroiRepositorio _heroiRepositorio;


        public HeroiService(IHeroiRepositorio repositorio, IServicoBroker servicoBroker) : base(repositorio)
        {
            _servicoBroker = servicoBroker;
            _heroiRepositorio = repositorio;
        }

        public override void Adicionar(Heroi entidade)
        {
            base.Adicionar(entidade);

            var obj = JsonConvert.SerializeObject(entidade);

            var msg = new MensagemDto() {
                Mensagem = obj,
                Fila = "fila-base2",
                Exchange = "heroi"
            };

            SalvarIntegracao(msg);

            _heroiRepositorio.SaveChanges();
        }

        public void SalvarIntegracao(MensagemDto dto)
        {
            _servicoBroker.EnviaMensagem(dto);
        }
    }
}
