﻿using Marvel.Dominio.Interfaces.Repositorios;
using Marvel.Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Servicos
{
    public class ServiceBase<T> : IDisposable, IServiceBase<T> where T : class
    {
        private readonly IRepositorioBase<T> _repository;

        public ServiceBase(IRepositorioBase<T> repositorio)
        {
           _repository = repositorio;
        }

        public virtual void Adicionar(T entidade)
        {
            _repository.Adicionar(entidade);
        }

        public virtual void Alterar(T entidade)
        {
            _repository.Alterar(entidade);
        }

        public void Dispose()
        {
            _repository.Dispose();
        }

        public virtual void Excluir(Guid id)
        {
            _repository.Excluir(id);
        }

        public T ObterPeloId(Guid id)
        {
            return _repository.ObterPeloId(id);
        }

        public IEnumerable<T> ObterTodos()
        {
            return _repository.ObterTodos();
        }
    }
}
