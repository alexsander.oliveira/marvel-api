﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Dominio.Entidades
{
    public abstract class EntidadeBase<T>
    {
        public Guid Id { get; protected set; }
    }
}
