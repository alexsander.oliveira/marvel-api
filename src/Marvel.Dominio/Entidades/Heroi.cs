﻿using System;

namespace Marvel.Dominio.Entidades
{
    public class Heroi : EntidadeBase<Heroi> 
    {
        public string Nome { get; set; }

        public Heroi(string nome)
        {
            base.Id = Guid.NewGuid();
            this.Nome = nome;
        }

        public Heroi(Guid id, string nome)
        {
            this.Id = id;
            this.Nome = nome;
        }

        public override string ToString()
        {
            return "Id: " + this.Id + " - Nome: " + this.Nome;
        }
    }
}
