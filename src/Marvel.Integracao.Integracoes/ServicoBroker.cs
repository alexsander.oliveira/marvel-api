﻿using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using Marvel.Integracao.Integracoes.Interfaces;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System.IO;
using System.Text;

namespace Marvel.Integracao.Integracoes
{
    public class ServicoBroker : IServicoBroker
    {
        public readonly ConnectionFactory _connectionFactory;

        public ServicoBroker()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var configurations = config.GetSection("RabbitMQConfigurations");

            _connectionFactory = new ConnectionFactory()
            {
                HostName = configurations.GetSection("HostName").Value,
                Port = int.Parse(configurations.GetSection("Port").Value),
                UserName = configurations.GetSection("UserName").Value,
                Password = configurations.GetSection("Password").Value,
                VirtualHost = configurations.GetSection("VirtualHost").Value,
                Protocol = Protocols.DefaultProtocol
            };
        }

        public void EnviaMensagem(MensagemDto dto)
        {
            using (var connection = _connectionFactory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: dto.Fila,
                                         durable: false, //quando eu desligo ou reinicio 
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    var body = Encoding.UTF8.GetBytes(dto.Mensagem);

                    channel.BasicPublish(exchange: dto.Exchange,
                                         routingKey: "",
                                         basicProperties: null,
                                         body: body);
                }
            }
        }
    }
}
