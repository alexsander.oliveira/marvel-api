﻿using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Integracao.Integracoes.Interfaces
{
    public interface IServicoBroker
    {
        void EnviaMensagem(MensagemDto dto);
    }
}
