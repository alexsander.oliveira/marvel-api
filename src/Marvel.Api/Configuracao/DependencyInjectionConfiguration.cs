﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Marvel.Infraestrutura.Transversao.InversaoDeControle;

namespace Marvel.Api.Configuracao
{
    public static class DependencyInjectionConfiguration
    {
        public static void AddDIConfiguration(this IServiceCollection services)
        {
            Inversao.RegistrarServicos(services);
        }
    }
}
