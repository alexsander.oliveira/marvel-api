﻿using AutoMapper;
using Marvel.Aplicacao.Interfaces;
using Marvel.Dominio.Entidades;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Marvel.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeroiController : ControllerBase
    {
        public readonly IHeroiAplicacaoService _heroiAplicacaoService;
        public readonly IMapper _mapper; 

        public HeroiController(IHeroiAplicacaoService heroiAplicacaoService, IMapper mapper)
        {
            _heroiAplicacaoService = heroiAplicacaoService;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<Heroi> Get()
        {
            return  _heroiAplicacaoService.ObterTodos();
        }

        [HttpPost]
        public object Post([FromBody]HeroiDto dto)
        {
            var entidade = _mapper.Map<Heroi>(dto);

            _heroiAplicacaoService.Adicionar(entidade);

            return new {
                Resultado = "Registro incluído com sucesso"
            };
        }

    }
}
