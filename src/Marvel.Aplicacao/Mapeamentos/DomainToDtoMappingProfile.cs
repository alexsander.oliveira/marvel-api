﻿using AutoMapper;
using Marvel.Dominio.Entidades;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Aplicacao.Mapeamentos
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<HeroiDto, Heroi>();
        }
    }
}
