﻿using AutoMapper;
using Marvel.Dominio.Entidades;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Aplicacao.Mapeamentos
{
    public class DtoToDomainMappingProfile : Profile
    {
        public DtoToDomainMappingProfile()
        {
            CreateMap<Heroi, HeroiDto>();
        }
    }
}
