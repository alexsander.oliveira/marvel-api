﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace Marvel.Aplicacao.Mapeamentos
{
    public class AutoMapperConfiguracao
    {
        public static MapperConfiguration RegistrarMappings()
        {
            return new MapperConfiguration(ps =>
            {
                ps.AddProfile(new DomainToDtoMappingProfile());
                ps.AddProfile(new DtoToDomainMappingProfile());
            });
        }

    }
}
