﻿using Marvel.Aplicacao.Interfaces;
using Marvel.Dominio.Entidades;
using Marvel.Dominio.Interfaces.Servicos;

namespace Marvel.Aplicacao.Servicos
{
    public class HeroiAplicacaoService : AplicacaoServiceBase<Heroi>, IHeroiAplicacaoService
    {
        public readonly IHeroiService _heroiService;

        public HeroiAplicacaoService(IHeroiService heroiService) : base(heroiService)
        {
            _heroiService = heroiService;
        }

        public override void Adicionar(Heroi entidade)
        {
            _heroiService.Adicionar(entidade);
        }
    }
}
