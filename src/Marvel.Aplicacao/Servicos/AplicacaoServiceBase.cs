﻿using Marvel.Aplicacao.Interfaces;
using Marvel.Dominio.Interfaces.Servicos;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Aplicacao.Servicos
{
    public class AplicacaoServiceBase<T> : IAplicacaoServiceBase<T> where T : class
    {
        public readonly IServiceBase<T> _serviceBase;

        public AplicacaoServiceBase(IServiceBase<T> service)
        {
            _serviceBase = service;
        }

        public virtual void Adicionar(T entidade)
        {
            _serviceBase.Adicionar(entidade);
        }

        public virtual void Alterar(T entidade)
        {
            _serviceBase.Alterar(entidade);
        }

        public virtual void Excluir(Guid id)
        {
            _serviceBase.Excluir(id);
        }

        public virtual T ObterPeloId(Guid id)
        {
            return _serviceBase.ObterPeloId(id);
        }

        public virtual IEnumerable<T> ObterTodos()
        {
            return _serviceBase.ObterTodos();
        }
    }
}
