﻿using Marvel.Dominio.Entidades;
using Marvel.Infraestrutura.Transversal.Dto.Integracoes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Aplicacao.Interfaces
{
    public interface IHeroiAplicacaoService : IAplicacaoServiceBase<Heroi>
    {
    }
}
