﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Aplicacao.Interfaces
{
    public interface IAplicacaoServiceBase<T> where T : class
    {
        void Adicionar(T entidade);
        void Alterar(T entidade);
        void Excluir(Guid id);
        IEnumerable<T> ObterTodos();
        T ObterPeloId(Guid id);
    }
}
