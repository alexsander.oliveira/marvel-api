﻿using Marvel.Dominio.Interfaces.Repositorios;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;
using System;
using System.Collections.Generic;
using System.Text;
using Marvel.Infraestrutura.Dados.Repositorio.Mapeamentos;
using Marvel.Dominio.Entidades;

namespace Marvel.Infraestrutura.Dados.Repositorio.Contexto
{
    public class Context : DbContext
    {
        public DbSet<Heroi> Heroi { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region FluentAPI

            modelBuilder.ApplyConfiguration(new HeroiMap());

            #endregion

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));

            base.OnConfiguring(optionsBuilder);
        }
    }
}
