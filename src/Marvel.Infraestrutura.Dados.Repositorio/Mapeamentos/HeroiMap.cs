﻿using Marvel.Dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Infraestrutura.Dados.Repositorio.Mapeamentos
{
    public class HeroiMap : IEntityTypeConfiguration<Heroi>
    {
        public void Configure(EntityTypeBuilder<Heroi> builder)
        {
            builder.ToTable("heroi");

            builder.Property(e => e.Id)
               .HasColumnName("UK_ID")
               .IsRequired();

            builder.Property(e => e.Nome)
                .HasColumnName("Nome")
                .IsRequired();
        }
    }
}
