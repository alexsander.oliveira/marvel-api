﻿using Marvel.Dominio.Entidades;
using Marvel.Dominio.Interfaces.Repositorios;
using Marvel.Infraestrutura.Dados.Repositorio.Contexto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Marvel.Infraestrutura.Dados.Repositorio.Repositorios
{
    public class HeroiRepositorio : RepositorioBase<Heroi>, IHeroiRepositorio
    {
        public HeroiRepositorio(Context contexto) : base(contexto)
        {
        }
    }
}
