﻿using Marvel.Dominio.Entidades;
using Marvel.Dominio.Interfaces.Repositorios;
using Marvel.Infraestrutura.Dados.Repositorio.Contexto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Marvel.Infraestrutura.Dados.Repositorio.Repositorios
{
    public class RepositorioBase<T> : IRepositorioBase<T> where T : EntidadeBase<T>
    {
        protected Context _contexto;
        protected DbSet<T> _dbSet;

        public RepositorioBase(Context contexto)
        {
            _contexto = contexto;
            _dbSet = _contexto.Set<T>();
        }

        public void Adicionar(T entidade)
        {
            _contexto.Add(entidade);
        }

        public void Alterar(T entidade)
        {
            _contexto.Update(entidade);
        }

        public void Excluir(Guid id)
        {
           _contexto.Remove(_dbSet.Find(id));
        }

        public T ObterPeloId(Guid id)
        {
            return _dbSet.AsNoTracking().FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<T> ObterTodos()
        {
            return _dbSet.AsNoTracking().ToList();
        }
        public void Dispose()
        {
            _contexto.Dispose();
        }

        public int SaveChanges()
        {
            return _contexto.SaveChanges();
        }
    }
}
